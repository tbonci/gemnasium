FROM golang:1.15-alpine AS build

ENV CGO_ENABLED=0

WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    PATH_TO_MODULE=`go list -m` && \
    go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer

FROM node:11-alpine

ENV PYTHON_PIP_VERSION 19.3
ENV PYTHON_GET_PIP_URL https://github.com/pypa/get-pip/raw/65986a26949050d26e6ec98915da4aade8d8679d/get-pip.py
ENV PYTHON_GET_PIP_SHA256 8d412752ae26b46a39a201ec618ef9ef7656c5b2d8529cdcbe60cd70dc94f40c

COPY vrange /vrange
ENV VRANGE_DIR="/vrange"

ARG GEMNASIUM_DB_LOCAL_PATH="/gemnasium-db"
ARG GEMNASIUM_DB_REMOTE_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db.git"
ARG GEMNASIUM_DB_WEB_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db"
ARG GEMNASIUM_DB_REF_NAME="master"

ENV GEMNASIUM_DB_LOCAL_PATH $GEMNASIUM_DB_LOCAL_PATH
ENV GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_REMOTE_URL
ENV GEMNASIUM_DB_WEB_URL $GEMNASIUM_DB_WEB_URL
ENV GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REF_NAME

RUN \
	# gemnasium-db
	apk add --no-cache git && \
	git clone --branch $GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_LOCAL_PATH && \
	\
	# vrange/php dependencies
	apk add --no-cache php7 php7-dom php7-ctype php7-tokenizer php7-xmlwriter php7-xml composer && \
	composer install -d "$VRANGE_DIR/php" && \
	\
	# vrange/gem dependencies
	apk add --no-cache git ruby ruby-json && \
	\
	# vrange/python dependencies
	apk add --no-cache git python3 && \
	wget -O get-pip.py "$PYTHON_GET_PIP_URL" && \
	echo "$PYTHON_GET_PIP_SHA256 *get-pip.py" | sha256sum -c - && \
	python3 get-pip.py --disable-pip-version-check --no-cache-dir "pip==$PYTHON_PIP_VERSION" && \
	pip install -r "$VRANGE_DIR/python/requirements.txt" && \
	\
	# vrange/npm dependencies
	yarn --cwd "$VRANGE_DIR/npm/yarn.lock" && \
	\
	# vrange/nuget dependencies
	apk add --no-cache libintl && \
	\
        # ensure we have the most recent versions of all installed packages
        apk upgrade --update-cache --available  && \
	\
	echo "done."

COPY --from=build /analyzer /analyzer

ENTRYPOINT []
CMD ["/analyzer", "run"]
