package advisory

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"testing"

	log "github.com/sirupsen/logrus"
)

func TestRepo_SatisfyPackageTypes(t *testing.T) {
	repo := Repo{Path: "testdata"}

	t.Run("ok", func(t *testing.T) {
		if err := repo.SatisfyPackageType("gem"); err != nil {
			t.Error(err)
		}
	})

	t.Run("no advisory", func(t *testing.T) {
		err := repo.SatisfyPackageType("no_advisory")
		want := ErrNoAdvisoryForPackageType{PackageType: "no_advisory"}
		if !reflect.DeepEqual(want, err) {
			t.Errorf("Wrong error. Expected %#v but got %#v", want, err)
		}
	})

	t.Run("no directory", func(t *testing.T) {
		err := repo.SatisfyPackageType("missing")
		want := ErrNoPackageTypeDir{PackageType: "missing"}
		if !reflect.DeepEqual(want, err) {
			t.Errorf("Wrong error. Expected %#v but got %#v", want, err)
		}
	})
}

func TestRepo_PackageAdvisories(t *testing.T) {
	testCases := []struct {
		name string
		pkg  Package
		want []string
	}{
		{
			name: "no advisory",
			pkg:  Package{Type: "no_advisory", Name: "package"},
			want: []string{},
		},
		{
			name: "find gem",
			pkg:  Package{Type: "gem", Name: "actionmovie"},
			want: []string{
				"gem/actionmovie/GMS-2019-minimal.yml",
			},
		},
		{
			name: "find pypi advisories",
			pkg:  Package{Type: "pypi", Name: "cryptography"},
			want: []string{
				"pypi/cryptography/CVE-2016-9243.YAML",
				"pypi/cryptography/CVE-2018-10903.yml",
			},
		},
		{
			name: "python matching rules",
			pkg:  Package{Type: "pypi", Name: "Django_server"},
			want: []string{
				"pypi/django-server/skcsirt-sa-20170909-pypi.yml",
			},
		},
		{
			name: "nested directories",
			pkg:  Package{Type: "maven", Name: "ca.uhn.hapi.fhir/hapi-fhir"},
			want: []string{
				"maven/ca.uhn.hapi.fhir/hapi-fhir/CVE-2019-12741.yml",
			},
		},
	}

	repo := Repo{Path: "testdata"}

	for _, c := range testCases {
		t.Run(c.name, func(t *testing.T) {
			got, err := repo.PackageAdvisories(c.pkg)
			if err != nil {
				t.Fatalf("Unexpected error when getting advisories: %#v", err)
			}
			if !reflect.DeepEqual(got, c.want) {
				t.Errorf("Wrong result. Expected %#v but got %#v", c.want, got)
			}
		})
	}

	t.Run("error when missing package", func(t *testing.T) {
		pkg := Package{Type: "pypi", Name: "xyz"}
		_, err := repo.PackageAdvisories(pkg)
		want := ErrNoPackageDir{Package: pkg}
		if !reflect.DeepEqual(want, err) {
			t.Errorf("Wrong error. Expected %#v but got %#v", want, err)
		}
	})

	t.Run("error when ambiguous pypi package dir", func(t *testing.T) {
		pkg := Package{Type: "pypi", Name: "elastic-apm"}
		_, err := repo.PackageAdvisories(pkg)
		want := ErrAmbiguousPackageDir{Package: pkg}
		if !reflect.DeepEqual(want, err) {
			t.Errorf("Wrong error. Expected %#v but got %#v", want, err)
		}
	})
}

func TestRepo_Advisory(t *testing.T) {
	repo := Repo{
		Path:    "testdata",
		WebURL:  "https://gitlab.com/gitlab-org/security-products/gemnasium-db",
		RefName: "dev",
	}

	got, err := repo.Advisory("gem/actionmovie/GMS-2019-minimal.yml")
	if err != nil {
		t.Fatal(err)
	}
	want := &Advisory{
		Identifier:     "GMS-2019-minimal",
		Title:          "Unsafe UPDATE query",
		Description:    "There is a vulnerability in some UPDATE query.",
		DisclosureDate: "2019-10-30",
		AffectedRange:  "*",
		Package: Package{
			Type: "gem",
			Name: "actionmovie",
		},
		URL: "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/dev/gem/actionmovie/GMS-2019-minimal.yml",
	}
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected %#v but got %#v", want, got)
	}
}

func BenchmarkRepo_PackageAdvisories(b *testing.B) {
	cases := []struct {
		numPackages int
		numFiles    int
	}{
		{1, 10},
		{10, 10},
		{100, 10},
		{100, 100},
	}

	cleanupDirs := make([]string, 0)
	for _, c := range cases {
		base, advisory, err := prepareTestData(c.numPackages, c.numFiles)
		if err != nil {
			log.Errorf("error preparing test data: %s\n", err)
			return
		}
		cleanupDirs = append(cleanupDirs, base)
		b.Run(fmt.Sprintf("run with %d packages and %d files", c.numPackages, c.numFiles), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				repo := Repo{Path: base}
				_, _ = repo.Advisory(advisory)
			}
		})
	}
	defer func() {
		for _, dir := range cleanupDirs {
			err := os.RemoveAll(dir)
			if err != nil {
				log.Warnf("Error deleting basedir for test %s\n", dir)
			}
		}
	}()
}

func prepareTestData(numPackages int, numFiles int) (basedir string, advisory string, err error) {
	tmpdir := os.TempDir()
	basedir, err = ioutil.TempDir(tmpdir, "gemnasium-repo-test")
	if err != nil {
		return
	}
	pkgTypes := []string{"gem", "pypi"}
	for _, pkgType := range pkgTypes {
		path := filepath.Join(basedir, pkgType)
		err = os.Mkdir(path, 0755)
		if err != nil {
			log.Errorf("Unable to create path for benchmark test %s %s\n", path, err)
			return
		}
		for i := 0; i < numPackages; i++ {
			path := filepath.Join(path, fmt.Sprintf("%d", i))
			err = os.Mkdir(path, 0755)
			if err != nil {
				log.Errorf("Unable to create path for benchmark test %s %s\n", path, err)
				return
			}
			for j := 0; j < numFiles; j++ {
				name := fmt.Sprintf("%d.yml", j)
				path := filepath.Join(path, name)
				_, err = os.Create(path)
				if err != nil {
					log.Errorf("Unable to create path for benchmark test %s %s\n", path, err)
					return
				}
				if j == 0 {
					advisory, err = filepath.Rel(basedir, path)
					if err != nil {
						log.Errorf("Unable to get relative path %s %s\n", path, err)
					}
				}
			}
		}
	}
	return
}
