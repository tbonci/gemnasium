module gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/sirupsen/logrus v1.4.2
	github.com/spiegel-im-spiegel/go-cvss v0.2.1
	github.com/stretchr/testify v1.4.0
	github.com/umisama/go-cvss v0.0.0-20150430082624-a4ad666ead9b
	github.com/urfave/cli v1.22.1
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.16.0
	gonum.org/v1/gonum v0.8.1
	gopkg.in/yaml.v2 v2.3.0
)

go 1.13
