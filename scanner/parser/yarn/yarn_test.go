package yarn

import (
	"encoding/json"
	"os"
	"reflect"
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

func TestYarn(t *testing.T) {
	t.Run("regexps", func(t *testing.T) {
		var tcs = []struct {
			name   string
			Regexp *regexp.Regexp
			input  string
			want   []string
		}{
			{
				"regexFormatVersion",
				regexFormatVersion,
				`# yarn lockfile v1`,
				[]string{"1"},
			},
			{
				"regexSpecList",
				regexSpecList,
				`"mime-db@>= 1.24.0 < 2", mime-db@~1.25.0, mine-db@1.25.0:`,
				[]string{`"mime-db@>= 1.24.0 < 2", mime-db@~1.25.0, mine-db@1.25.0`},
			},
			{
				"regexSpec",
				regexSpec,
				`"mime-db@>= 1.24.0 < 2"`,
				[]string{"mime-db", ">= 1.24.0 < 2"},
			},
			{
				"regexVersion",
				regexVersion,
				`  version "4.0.0"`,
				[]string{"4.0.0"},
			},
			{
				"regexDependency",
				regexDependency,
				`    normalize-path "^2.0.1"`,
				[]string{"normalize-path", "^2.0.1"},
			},
			{
				"regexDependency escaped name",
				regexDependency,
				`    "@nuxtjs/cssnano" "1.2.3"`,
				[]string{"@nuxtjs/cssnano", "1.2.3"},
			},
			{
				"regexDependency latest",
				regexDependency,
				`    normalize-path latest`,
				[]string{"normalize-path", "latest"},
			},
			{
				"regexDependency conjunction",
				regexDependency,
				`    mime-db ">= 1.40.0 < 2"`,
				[]string{"mime-db", ">= 1.40.0 < 2"},
			},
			{
				"regexDependency disjunction",
				regexDependency,
				`    js-tokens "^3.0.0 || ^4.0.0"`,
				[]string{"js-tokens", "^3.0.0 || ^4.0.0"},
			},
		}

		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				all := tc.Regexp.FindStringSubmatch(tc.input)
				if len(all) < 2 {
					t.Fatalf("Expected regexp to capture at least one string in %s", tc.input)
				}
				got := all[1:]
				if !reflect.DeepEqual(got, tc.want) {
					t.Fatalf("Expected regexp to capture\n%#v\nbut got\n%#v", tc.want, got)
				}
			})
		}
	})

	t.Run("Parse", func(t *testing.T) {
		for _, tc := range []string{"simple", "big"} {
			t.Run(tc, func(t *testing.T) {
				// Load fixture
				fixture, err := os.Open("fixtures/" + tc + "/yarn.lock")
				if err != nil {
					t.Error("Can't open fixture file", err)
				}
				defer fixture.Close()
				pkgs, deps, err := Parse(fixture)
				if err != nil {
					t.Fatal(err)
				}

				t.Run("packages", func(t *testing.T) {
					// Load expected output
					expect, err := os.Open("expect/" + tc + "/packages.json")
					if err != nil {
						t.Fatal("Can't open expect file", err)
					}
					defer expect.Close()
					var want []parser.Package
					err = json.NewDecoder(expect).Decode(&want)
					if err != nil {
						t.Fatal(err)
					}

					// TODO: replace with assert.ElementsMatch(t, got, want) when migrating to stretchr/testify
					got := pkgs
					if !reflect.DeepEqual(got, want) {
						t.Errorf("Wrong result. Expected\n%v\nbut got\n%v", want, got)
					}
				})

				t.Run("dependencies", func(t *testing.T) {
					// Load expected output
					expect, err := os.Open("expect/" + tc + "/dependencies.json")
					if err != nil {
						t.Fatal("Can't open expect file", err)
					}
					defer expect.Close()
					var want []parser.Dependency
					err = json.NewDecoder(expect).Decode(&want)
					if err != nil {
						t.Fatal(err)
					}

					assert.ElementsMatch(t, deps, want)
				})
			})
		}
	})
}
