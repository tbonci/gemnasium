package scanner

import (
	"encoding/json"
	"flag"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"

	"github.com/urfave/cli"

	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/gemfile"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/go"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/pipdeptree"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/yarn"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/advisory"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange/golang"
)

func init() {
	// register version range resolvers
	for _, syntax := range []string{"npm", "gem", "python"} {
		if err := vrange.RegisterCmd(syntax, "../vrange/semver/vrange-"+runtime.GOOS, "npm"); err != nil {
			panic(err)
		}
	}
	vrange.Register("go", &golang.Resolver{})
}

func TestScanner(t *testing.T) {

	// scanner
	set := flag.NewFlagSet("test", 0)
	set.String("gemnasium-db-local-path", "testdata/gemnasium-db", "doc")
	set.Bool("gemnasium-db-update", false, "doc")
	c := cli.NewContext(nil, set, nil)
	scanner, err := NewScanner(c)
	if err != nil {
		t.Fatal(err)
	}

	t.Run("ScanDir", func(t *testing.T) {
		dir := "testdata/depfiles"
		got, err := scanner.ScanDir(dir)
		if err != nil {
			t.Fatal(err)
		}

		// ignore dependencies because pointers cannot be compared
		for i := range got {
			got[i].Dependencies = nil
		}

		want := []File{}
		checkExpectedFile(t, &want, &got, filepath.Join(dir, "scandir.json"))
	})

	t.Run("ScanFile", func(t *testing.T) {
		t.Run("Gemfile.lock", func(t *testing.T) {
			dir := "testdata/depfiles"
			name := "bundler/Gemfile.lock"
			path := filepath.Join(dir, name)
			alias := "bundler/Gemfile"
			got, err := scanner.ScanFile(path, alias)
			if err != nil {
				t.Fatal(err)
			}
			want := File{}
			checkExpectedFile(t, &want, got, filepath.Join(dir, "scanfile-gem.json"))
		})

		t.Run("pipdeptree.json", func(t *testing.T) {
			dir := "testdata/depfiles"
			name := "pypi/pipdeptree.json"
			path := filepath.Join(dir, name)
			alias := "pypi/pipdeptree.json"
			got, err := scanner.ScanFile(path, alias)
			if err != nil {
				t.Fatal(err)
			}
			want := File{}
			checkExpectedFile(t, &want, got, filepath.Join(dir, "scanfile-pypi.json"))
		})

		t.Run("go.sum", func(t *testing.T) {
			dir := "testdata/depfiles"
			name := "go/go.sum"
			path := filepath.Join(dir, name)
			alias := "go/go.sum"
			got, err := scanner.ScanFile(path, alias)
			if err != nil {
				t.Fatal(err)
			}
			want := File{}
			checkExpectedFile(t, &want, got, filepath.Join(dir, "scanfile-go.json"))
		})
	})

	t.Run("ScanReader", func(t *testing.T) {
		dir := "testdata/depfiles"
		path := "yarn/yarn.lock"
		f, err := os.Open(filepath.Join(dir, path))
		if err != nil {
			t.Fatal(err)
		}
		got, err := scanner.ScanReader(f, "yarn.lock", path)
		if err != nil {
			t.Fatal(err)
		}
		// ignore dependencies because pointers cannot be compared
		got.Dependencies = nil
		want := File{}
		checkExpectedFile(t, &want, got, filepath.Join(dir, "scanreader-npm.json"))
	})
}

func TestScanner_GemOnlyRepo(t *testing.T) {

	// scanner
	set := flag.NewFlagSet("test", 0)
	set.String("gemnasium-db-local-path", "testdata/gemnasium-db-gem", "doc")
	set.Bool("gemnasium-db-update", false, "doc")
	c := cli.NewContext(nil, set, nil)
	scanner, err := NewScanner(c)
	if err != nil {
		t.Fatal(err)
	}

	var tcs = []struct {
		file    string
		wantErr error
	}{
		{
			file:    "bundler/Gemfile.lock",
			wantErr: nil,
		},
		{
			file:    "pypi/pipdeptree.json",
			wantErr: advisory.ErrNoAdvisoryForPackageType{PackageType: "pypi"},
		},
		{
			file:    "yarn/yarn.lock",
			wantErr: advisory.ErrNoPackageTypeDir{PackageType: "npm"},
		},
	}

	dir := "testdata/depfiles"
	for _, tc := range tcs {
		t.Run(tc.file, func(t *testing.T) {
			path := filepath.Join(dir, tc.file)
			_, err := scanner.ScanFile(path, tc.file)
			if !reflect.DeepEqual(tc.wantErr, err) {
				t.Errorf("wrong result. expected '%v' but got '%v'", tc.wantErr, err)
			}
		})
	}
}

func checkExpectedFile(t *testing.T, want, got interface{}, path string) {
	// look for file containing expected JSON document
	if _, err := os.Stat(path); err != nil {
		createExpectedFile(t, got, path)
	} else {
		compareToExpectedFile(t, want, got, path)
	}
}

func compareToExpectedFile(t *testing.T, want, got interface{}, path string) {
	f, err := os.Open(path)
	if err != nil {
		t.Fatal(err)
	}
	defer f.Close()

	err = json.NewDecoder(f).Decode(want)
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("result doesn't match content of %s", path)
	}
}

func createExpectedFile(t *testing.T, got interface{}, path string) {
	// make test fail
	t.Errorf("creating JSON document: %s", path)

	// create directory for file
	if err := os.MkdirAll(filepath.Dir(path), 0755); err != nil {
		t.Fatal(err)
	}

	// create file
	f, err := os.OpenFile(path, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		t.Fatal(err)
	}
	defer f.Close()

	// write JSON doc
	encoder := json.NewEncoder(f)
	encoder.SetIndent("", "  ")
	encoder.SetEscapeHTML(false)
	if err := encoder.Encode(got); err != nil {
		t.Fatal(err)
	}
}
