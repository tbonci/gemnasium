package scanner

import (
	"fmt"
	"io"
	"os"
	"path/filepath"

	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/advisory"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/finder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange"
)

const (
	flagPrefix      = "gemnasium-"
	flagDBUpdate    = flagPrefix + "db-update"
	flagDBLocalPath = flagPrefix + "db-local-path"
	flagDBRemoteURL = flagPrefix + "db-remote-url"
	flagDBWebURL    = flagPrefix + "db-web-url"
	flagDBRefName   = flagPrefix + "db-ref-name"
	flagIgnoredDirs = flagPrefix + "ignore-dirs"

	envVarPrefix      = "GEMNASIUM_"
	envVarDBUpdate    = envVarPrefix + "DB_UPDATE"
	envVarDBLocalPath = envVarPrefix + "DB_LOCAL_PATH"
	envVarDBRemoteURL = envVarPrefix + "DB_REMOTE_URL"
	envVarDBWebURL    = envVarPrefix + "DB_WEB_URL"
	envVarDBRefName   = envVarPrefix + "DB_REF_NAME"
	envVarIgnoredDirs = envVarPrefix + "IGNORED_DIRS,SEARCH_IGNORED_DIRS"
)

var defaultIgnoredDirs = cli.StringSlice([]string{"node_modules", ".bundle", "vendor", ".git"})

// Flags generates new command line flags for the scanner
func Flags() []cli.Flag {
	return []cli.Flag{
		cli.BoolTFlag{
			Name:   flagDBUpdate,
			EnvVar: envVarDBUpdate,
			Usage:  "Update gemnasium-db git repo before scanning (default: true)",
		},
		cli.StringFlag{
			Name:   flagDBLocalPath,
			EnvVar: envVarDBLocalPath,
			Usage:  "Path of gemnasium-db git repo",
			Value:  "gemnasium-db",
		},
		cli.StringFlag{
			Name:   flagDBRemoteURL,
			EnvVar: envVarDBRemoteURL,
			Usage:  "Remote URL the local gemnasium-db git repo is synced with",
		},
		cli.StringFlag{
			Name:   flagDBWebURL,
			EnvVar: envVarDBWebURL,
			Usage:  "Web URL of the gemnasium-db GitLab project",
		},
		cli.StringFlag{
			Name:   flagDBRefName,
			EnvVar: envVarDBRefName,
			Usage:  "git reference the local gemnasium-db git repo is synced with",
		},
		cli.StringSliceFlag{
			Name:   flagIgnoredDirs,
			EnvVar: envVarIgnoredDirs,
			Usage:  "Directory to be ignored",
			Value:  &defaultIgnoredDirs,
		},
	}
}

// NewScanner parses command line arguments from a cli.Context and returns a new scanner
func NewScanner(c *cli.Context) (*Scanner, error) {
	f := finder.Finder{IgnoredDirs: c.StringSlice(flagIgnoredDirs)}

	// GitLab project and git repo for the advisories
	r := advisory.Repo{
		Path:      c.String(flagDBLocalPath),
		RemoteURL: c.String(flagDBRemoteURL),
		WebURL:    c.String(flagDBWebURL),
		RefName:   c.String(flagDBRefName),
	}

	// update advisory repo if requested
	if c.Bool(flagDBUpdate) {
		if err := r.Update(); err != nil {
			return nil, err
		}
	}

	return &Scanner{f, r}, nil
}

// Scanner contains a finder which determines which dirs to scan and repo
type Scanner struct {
	finder finder.Finder
	repo   advisory.Repo
}

// ScanDir scans a directory and returns all valid files for scanning
func (s Scanner) ScanDir(dir string) ([]File, error) {

	// find dependency files
	files, err := s.finder.FindFiles(dir)
	if err != nil {
		return nil, err
	}

	// parse dependency files
	depfiles := []File{}
	for _, file := range files {
		path := filepath.Join(file.RootDir, file.Path)
		f, err := os.Open(path)
		if err != nil {
			return nil, err
		}
		depfile, err := parseReader(f, file.Parser, file.Path)
		if err != nil {
			return nil, err
		}
		depfile.RootDir = dir
		depfiles = append(depfiles, *depfile)
	}
	// add affections to dependency files
	for i, depfile := range depfiles {
		aff, err := fileAffections(depfile, s.repo)
		if err != nil {
			return nil, err
		}
		depfiles[i].Affections = aff
	}

	return depfiles, nil
}

// ScanFile opens a file and calls ScanReader to perform the scan
func (s Scanner) ScanFile(path, alias string) (*File, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	fileType := filepath.Base(path)
	return s.ScanReader(f, fileType, alias)
}

// ScanReader parses a file with a parser determined by filetype
func (s Scanner) ScanReader(reader io.Reader, fileType, filePath string) (*File, error) {
	depParser := parser.Lookup(fileType)
	if depParser == nil {
		return nil, fmt.Errorf("no parser for file type %s, path %s", fileType, filePath)
	}
	depfile, err := parseReader(reader, *depParser, filePath)
	if err != nil {
		return nil, err
	}
	aff, err := fileAffections(*depfile, s.repo)
	if err != nil {
		return nil, err
	}
	depfile.Affections = aff
	return depfile, nil
}

func parseReader(reader io.Reader, depParser parser.Parser, filePath string) (*File, error) {
	pkgs, deps, err := depParser.Parse(reader)
	if err != nil {
		return nil, err
	}

	return &File{
		Path:           filePath,
		PackageManager: depParser.PackageManager,
		PackageType:    string(depParser.PackageType),
		Packages:       pkgs,
		Dependencies:   deps,
	}, nil
}

type queryTranslator func(vrange.Query, []advisory.Version) vrange.Query

func fileAffections(depfile File, repo advisory.Repo) ([]Affection, error) {
	pkgType := depfile.PackageType

	// version range resolver
	resolver, err := vrange.NewResolver(pkgTypeToResolverName(pkgType))
	if err != nil {
		return nil, err
	}

	// query translator
	var translateQuery queryTranslator
	switch t := resolver.(type) {
	default:
		// use identify function
		translateQuery = func(q vrange.Query, versionMeta []advisory.Version) vrange.Query {
			return q
		}
	case vrange.QueryTranslator:
		// delegate to resolver
		translateQuery = func(q vrange.Query, versionMeta []advisory.Version) vrange.Query {
			return t.TranslateQuery(q, versionMeta)
		}
	}

	// ensure repo has advisories matching the package type
	if err := repo.SatisfyPackageType(pkgType); err != nil {
		return nil, err
	}

	// collect possible affections, list version range queries
	affections := []Affection{}
	queries := []vrange.Query{}
	for _, dep := range depfile.Packages {

		// list package advisories
		pkg := advisory.Package{Type: pkgType, Name: dep.Name}
		paths, err := repo.PackageAdvisories(pkg)
		if _, ok := err.(advisory.ErrNoPackageDir); ok {
			continue // no advisories
		}
		if err != nil {
			return nil, err
		}

		// fetch advisories, append affections and queries
		for _, path := range paths {
			adv, err := repo.Advisory(path)
			if err != nil {
				return nil, err
			}
			aff := Affection{dep, *adv}
			affections = append(affections, aff)
			q := translateQuery(aff.query(), aff.Advisory.Versions)
			queries = append(queries, q)

		}
	}

	// resolve queries and tell if dependency version is in affected range
	result, err := resolver.Resolve(queries)
	if err != nil {
		return nil, err
	}

	// filter affections and keep those where the dependency version is in affected range
	filtered := []Affection{}
	for _, aff := range affections {
		q := translateQuery(aff.query(), aff.Advisory.Versions)
		ok, err := result.Satisfies(q)
		if err != nil {
			// TODO report error
			continue
		}
		if ok {
			filtered = append(filtered, aff)
		}
	}
	return filtered, nil
}

// pkgTypeToResolverName converts a package type to the name of a vrange resolver.
func pkgTypeToResolverName(pkgType string) string {
	switch pkgType {
	case "npm", "maven", "gem", "go":
		return pkgType
	case "packagist":
		return "php"
	case "pypi":
		return "python"
	default:
		return pkgType
	}
}
