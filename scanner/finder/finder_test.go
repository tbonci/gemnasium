package finder

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/gemfile"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/yarn"
)

// finderMatch is used to compare a returned File with an expectation.
// It's needed because File embeds a Parser and Parser.Parse can't be compared.
type finderMatch struct {
	Path        string
	RootDir     string
	PackageType parser.PackageType
}

func TestFinder(t *testing.T) {
	t.Run("FindFiles", func(t *testing.T) {
		t.Run("empty", func(t *testing.T) {
			finder := Finder{}
			_, err := finder.FindFiles("empty")
			if err == nil {
				t.Fatal("Expected error but got none")
			}
		})

		t.Run("fixtures", func(t *testing.T) {
			finder := Finder{IgnoredDirs: []string{"ignore", "skip"}}
			files, err := finder.FindFiles("fixtures")
			if err != nil {
				t.Fatal(err)
			}

			want := []finderMatch{
				{
					// filename alias
					Path:        "bundler/gems.locked",
					RootDir:     "fixtures",
					PackageType: "gem",
				},
				{
					// nested directory
					Path:        "nested/bundler/Gemfile.lock",
					RootDir:     "fixtures",
					PackageType: "gem",
				},
				{
					// nested directory
					Path:        "nested/yarn/yarn.lock",
					RootDir:     "fixtures",
					PackageType: "npm",
				},
				{
					Path:        "yarn/yarn.lock",
					RootDir:     "fixtures",
					PackageType: "npm",
				},
			}

			got := make([]finderMatch, len(files))
			for i, f := range files {
				got[i] = finderMatch{
					Path:        f.Path,
					RootDir:     f.RootDir,
					PackageType: f.Parser.PackageType,
				}
			}

			if !reflect.DeepEqual(want, got) {
				t.Errorf("Wrong result. Expecting:\n%+v\nbut got:\n%+v", want, got)
			}
		})
	})
}
