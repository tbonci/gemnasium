package plugin

import (
	"os"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

// Match checks that the file has an available parser
func Match(path string, info os.FileInfo) (bool, error) {
	if p := parser.Lookup(info.Name()); p != nil {
		return true, nil
	}
	return false, nil
}

func init() {
	plugin.Register("gemnasium", Match)
}
