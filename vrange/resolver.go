package vrange

import "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/advisory"

// Resolver is implemented by types that can evaluate queries.
type Resolver interface {
	Resolve([]Query) (*ResultSet, error) // Resolve evaluates queries
}

// NewResolver returns a resolver matching the given name.
func NewResolver(name string) (Resolver, error) {
	resolver := Lookup(name)
	if resolver == nil {
		return nil, ErrResolverNotFound{name, Resolvers()}
	}
	return resolver, nil
}

// QueryTranslator is implemented by types that can translate queries
// using version metadata.
type QueryTranslator interface {
	TranslateQuery(Query, []advisory.Version) Query
}
