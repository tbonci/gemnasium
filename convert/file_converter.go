package convert

import (
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

// DepPathMode is the strategy used to enable dependency paths
type DepPathMode int

const (
	// DepPathModeNone is when dependency paths are disabled
	DepPathModeNone = DepPathMode(iota)

	// DepPathModeAffected is when dependency paths are enabled
	// only for packages that are affected
	DepPathModeAffected

	// DepPathModeAll is when dependency paths are enabled
	// for all dependencies
	DepPathModeAll
)

// Config configures how a scanned file is converted.
type Config struct {
	DepPathMode DepPathMode // DepPathMode is the strategy used to enable dependency paths
	PrependPath string      // PrependPath is prepended to the file path
}

// setDepPathFunc is the type of a function that tells if the dependency path
// should be rendered for a given package
type setDepPathFunc func(parser.Package) bool

// NewFileConverter returns a new FileConverter
// where the package index and the dependency graph have been initialized if needed.
func NewFileConverter(f scanner.File, cfg Config) *FileConverter {
	var index *Index              // index used to turn packages into IIDs
	var graph *Graph              // graph used to calculate dependency paths
	var setDepIID bool            // true if all dependencies should have IIDs
	var setDepPath setDepPathFunc // true if given package should have a dependency path

	// configure setDepIID
	switch cfg.DepPathMode {
	case DepPathModeNone:
		setDepIID = false

	case DepPathModeAffected, DepPathModeAll:
		// set IIDs for all dependencies; a dependency might be referrenced
		// in the path to a vulnerable dependency if it's not vulnerable
		setDepIID = true

		// build index and graph
		index = NewIndex(f.Packages)
		graph = NewGraph(f.Packages, f.Dependencies, index)
	}

	// configure setDepPath
	switch cfg.DepPathMode {
	case DepPathModeNone:
		// never set dependency path
		setDepPath = func(parser.Package) bool {
			return false
		}

	case DepPathModeAffected:
		// build map of affected packages
		affectedMap := map[parser.Package]bool{}
		for _, aff := range f.Affections {
			affectedMap[aff.Dependency] = true
		}
		// set dependency path if package is affected
		setDepPath = func(pkg parser.Package) bool {
			_, affected := affectedMap[pkg]
			return affected
		}

	case DepPathModeAll:
		// always set dependency path
		setDepPath = func(parser.Package) bool {
			return true
		}
	}

	return &FileConverter{
		prependPath: cfg.PrependPath,
		setDepIID:   setDepIID,
		setDepPath:  setDepPath,
		File:        f,
		index:       index,
		graph:       graph,
	}
}

// FileConverter is used to render a file processed by the scanner
// into a list of vulnerability objects and a dependency file object,
// to be included in the JSON report.
type FileConverter struct {
	prependPath string
	setDepIID   bool
	setDepPath  setDepPathFunc
	File        scanner.File
	index       *Index
	graph       *Graph
}

// DependencyFile generates a dependency file object
// to be included in the JSON report.
func (c FileConverter) DependencyFile() issue.DependencyFile {
	return issue.DependencyFile{
		Path:           filepath.Join(c.prependPath, c.File.Path),
		Dependencies:   c.dependencies(),
		PackageManager: issue.PackageManager(c.File.PackageManager),
	}
}

// dependencies generates a slice of dependency objects
// to be included in the JSON report.
func (c FileConverter) dependencies() []issue.Dependency {
	deps := []issue.Dependency{}
	for _, pkg := range c.File.Packages {
		dep := issue.Dependency{
			Package: issue.Package{Name: pkg.Name},
			Version: pkg.Version,
		}

		if c.setDepIID {
			dep.IID = c.index.IDOf(pkg)
		}

		if c.setDepPath(pkg) {
			path := c.graph.PathTo(pkg)
			if len(path) == 0 {
				dep.Direct = true
			}
			for _, node := range path {
				ref := issue.DependencyRef{IID: node.IID}
				dep.DependencyPath = append(dep.DependencyPath, ref)
			}
		}

		deps = append(deps, dep)
	}
	return deps
}

// Vulnerabilities generates a slice of vulnerability objects
// to be included in the JSON report.
func (c FileConverter) Vulnerabilities() []issue.Issue {
	filePath := filepath.Join(c.prependPath, c.File.Path)
	vulns := make([]issue.Issue, len(c.File.Affections))
	for i, affection := range c.File.Affections {

		var iid uint
		pkg := affection.Dependency
		if c.setDepIID {
			iid = c.index.IDOf(pkg)
		}

		c := VulnerabilityConverter{
			FilePath:      filePath,
			Advisory:      affection.Advisory,
			Dependency:    pkg,
			DependencyIID: iid,
		}
		vulns[i] = c.Issue()
	}
	return vulns
}
